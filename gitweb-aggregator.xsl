<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns="http://www.w3.org/2005/Atom"
>
<xsl:output method="xml" indent="yes" encoding="UTF-8" />
<xsl:param name="SINCE" />
<xsl:param name="PREFIX" />
<xsl:param name="SUFFIX" />
<xsl:template match="/">
<feed>
<xsl:copy-of select="feeds/atom:header/*" />
<generator uri="https://code.mathr.co.uk/gitweb-aggregator">gitweb-aggregator</generator>
<!-- updated -->
<xsl:for-each select="feeds/atom:feed">
  <!-- sort by date descending -->
  <xsl:sort select="substring(atom:updated,  1, 4)" order="descending" />
  <xsl:sort select="substring(atom:updated,  6, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated,  9, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated, 12, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated, 15, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated, 18, 2)" order="descending" />
  <!-- take the first item -->
  <xsl:if test="position() = 1">
    <xsl:copy-of select="atom:updated" />
  </xsl:if>
</xsl:for-each>
<!-- entry -->
<xsl:for-each select="feeds/atom:feed/atom:entry">
  <!-- sort by date descending -->
  <xsl:sort select="substring(atom:updated,  1, 4)" order="descending" />
  <xsl:sort select="substring(atom:updated,  6, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated,  9, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated, 12, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated, 15, 2)" order="descending" />
  <xsl:sort select="substring(atom:updated, 18, 2)" order="descending" />
  <!-- filter by date >= SINCE -->
  <xsl:if test="
    substring(atom:updated,  1, 4) > substring($SINCE,  1, 4) or  (
    substring(atom:updated,  1, 4) = substring($SINCE,  1, 4) and (
    substring(atom:updated,  6, 2) > substring($SINCE,  6, 2) or  (
    substring(atom:updated,  6, 2) = substring($SINCE,  6, 2) and (
    substring(atom:updated,  9, 2) > substring($SINCE,  9, 2) or  (
    substring(atom:updated,  9, 2) = substring($SINCE,  9, 2) and (
    substring(atom:updated, 12, 2) > substring($SINCE, 12, 2) or  (
    substring(atom:updated, 12, 2) = substring($SINCE, 12, 2) and (
    substring(atom:updated, 15, 2) > substring($SINCE, 15, 2) or  (
    substring(atom:updated, 15, 2) = substring($SINCE, 15, 2) and (
    substring(atom:updated, 18, 2) >=substring($SINCE, 18, 2) ))))))))))
  ">
     <entry>
        <!-- include feed identifier in title -->
        <title type="html">[<xsl:value-of select="substring-before(substring-after(../atom:title, $PREFIX), $SUFFIX)" />] <xsl:value-of select="atom:title" /></title>
        <!-- include feed identifier as category -->
        <category term="{substring-before(substring-after(../atom:title, $PREFIX), $SUFFIX)}" />
        <xsl:copy-of select="atom:updated" />
        <xsl:copy-of select="atom:published" />
        <!-- ignore author email -->
        <author><xsl:copy-of select="atom:author/atom:name" /></author>
        <xsl:copy-of select="atom:link" />
        <xsl:copy-of select="atom:id" />
        <xsl:copy-of select="atom:content" />
        <source>
          <xsl:copy-of select="../atom:id" />
          <xsl:copy-of select="../atom:link" />
          <xsl:copy-of select="../atom:title" />
          <xsl:copy-of select="../atom:updated" />
        </source>
      </entry>
  </xsl:if>
</xsl:for-each>
</feed>
</xsl:template>
</xsl:stylesheet>
