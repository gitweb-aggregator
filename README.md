gitweb-aggregator
=================

Aggregate atom feeds from gitweb.  Useful if you have a gitweb instance,
and want to generate a feed for everything.


Usage
-----

Read the source.  Some things assume the URL layout presented by
code.mathr.co.uk, so you will probably need to customize things.


-- 
claude@mathr.co.uk
http://mathr.co.uk
