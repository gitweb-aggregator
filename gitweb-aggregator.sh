#!/bin/sh
self="$(dirname "$(readlink -e "${0}")")"
conf="$(readlink -e "${1}")"

# read configuration
if [ "x" = "x${1}" ]
then
  echo >&2 "usage: ${0} config-file"
  exit 1
fi
. "${conf}" || exit 1

# check configuration
[ "x" = "x${proto}"  ] && echo >&2 "error: 'proto' not set by '${conf}'"  && exit 1
[ "x" = "x${cache}"  ] && echo >&2 "error: 'cache' not set by '${conf}'"  && exit 1
[ "x" = "x${since}"  ] && echo >&2 "error: 'since' not set by '${conf}'"  && exit 1
[ "x" = "x${site}"   ] && echo >&2 "error: 'site' not set by '${conf}'"   && exit 1
[ "x" = "x${index}"  ] && echo >&2 "error: 'index' not set by '${conf}'"  && exit 1
# [ "x" = "x${prefix}" ] && echo >&2 "error: 'prefix' not set by '${conf}'" && exit 1
# [ "x" = "x${suffix}" ] && echo >&2 "error: 'suffix' not set by '${conf}'" && exit 1
[ "x" = "x${header}" ] && echo >&2 "error: 'header' not set by '${conf}'" && exit 1
[ "x" = "x${out}"    ] && echo >&2 "error: 'out' not set by '${conf}'"    && exit 1

# download feeds
wget -nv -P "${cache}" -x -nH "${proto}://${site}/${index}"
cat "${cache}/index.html${index}" |
while read repository author
do
  echo "'${proto}://${site}/${repository}/atom'"
done |
xargs wget -nv -N -P "${cache}" -x -nH

# aggregate feeds
(
  echo '<?xml version="1.0" encoding="utf-8"?>'
  echo '<feeds>'
  echo "${header}"
  cat "${cache}/index.html${index}" |
  while read repository author
  do
    # strip xml header line
    tail -n+2 < "${cache}/${repository}/atom"
  done
  echo '</feeds>'
) |
xsltproc \
  --stringparam SINCE "${since}" \
  --stringparam PREFIX "${prefix}" \
  --stringparam SUFFIX "${suffix}" \
  "${self}/gitweb-aggregator.xsl" - > "${cache}/commits.atom"

# update target only if changed
diff >/dev/null 2>/dev/null -q "${cache}/commits.atom" "${out}" ||
cp -af "${cache}/commits.atom" "${out}"
